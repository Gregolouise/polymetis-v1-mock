import * as services from '../../services/apiCRUD/ApiServices'
import { LPRWrapperHttp } from '../../libs/wrapper-lib'
import { CustomHttpResponse } from '../../libs/response-lib'
import { CustomHttpError } from '../../libs/errors-lib'

async function get (event) {
  if (event.queryStringParameters.lat === undefined || event.queryStringParameters.lng === undefined || event.queryStringParameters.radius === undefined) {
    throw new CustomHttpError('MissingQueryParams', 400)
  }
  const professionals = await services.getAll('mock-server-local-professionals')
  if (!professionals) {
    throw new CustomHttpError('NoProfessionalException', 404)
  } else {
    return new CustomHttpResponse(professionals, 200)
  }
}

exports.main = LPRWrapperHttp(get)
