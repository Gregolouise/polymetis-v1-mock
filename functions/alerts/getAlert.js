import * as services from '../../services/apiCRUD/ApiServices'
import { LPRWrapperHttp } from '../../libs/wrapper-lib'
import { CustomHttpResponse } from '../../libs/response-lib'
import { CustomHttpError } from '../../libs/errors-lib'

async function get (event) {
  const alert = await services.getById('mock-server-local-alerts', event.pathParameters.alertId)
  if (!alert) {
    throw new CustomHttpError('NoAlertException', 404)
  } else {
    return new CustomHttpResponse(alert, 200)
  }
}

exports.main = LPRWrapperHttp(get)
