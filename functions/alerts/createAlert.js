import * as services from '../../services/apiCRUD/ApiServices'
import { LPRWrapperHttp } from '../../libs/wrapper-lib'
import { CustomHttpResponse } from '../../libs/response-lib'
import { CustomHttpError } from '../../libs/errors-lib'
import uuid from 'uuid'

async function get (event) {
  const alertId = uuid.v1()
  event.body.id = alertId
  const alert = await services.put('mock-server-local-alerts', event.body)
  if (!alert) {
    throw new CustomHttpError('NoAlertException', 404)
  } else {
    return new CustomHttpResponse({ id: alertId }, 201)
  }
}

exports.main = LPRWrapperHttp(get)
