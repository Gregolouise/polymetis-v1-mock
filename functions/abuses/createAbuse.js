import * as services from '../../services/apiCRUD/ApiServices'
import { LPRWrapperHttp } from '../../libs/wrapper-lib'
import { CustomHttpResponse } from '../../libs/response-lib'
import { CustomHttpError } from '../../libs/errors-lib'
import uuid from 'uuid'

async function get (event) {
  const alert = await services.getById('mock-server-local-alerts', event.pathParameters.alertId)
  if (!alert) {
    throw new CustomHttpError('NoAlertException', 404)
  } else {
    const abuseId = uuid.v1()
    event.body.id = abuseId
    await services.put('mock-server-local-abuses', event.body)
    return new CustomHttpResponse({ id: abuseId }, 201)
  }
}

exports.main = LPRWrapperHttp(get)
