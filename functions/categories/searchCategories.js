import * as services from '../../services/apiCRUD/ApiServices'
import { LPRWrapperHttp } from '../../libs/wrapper-lib'
import { CustomHttpResponse } from '../../libs/response-lib'
import { CustomHttpError } from '../../libs/errors-lib'

async function get (event) {
  const categories = await services.getAll('mock-server-local-categories')
  if (!categories) {
    throw new CustomHttpError('NoCategoriesException', 404)
  } else {
    return new CustomHttpResponse(categories, 200)
  }
}

exports.main = LPRWrapperHttp(get)
