import * as dynamoDbLib from './../../libs/dynamodb-lib'

export async function getById (table, id) {
  const result = await dynamoDbLib.call('get', {
    TableName: table,
    Key: {
      id: id
    }
  })

  return result.Item
}

export async function get (table, hashKeyName, hashKeyValue) {
  const params = {
    TableName: table,
    Key: {}
  }
  params.Key[hashKeyName] = hashKeyValue
  const result = await dynamoDbLib.call('get', params)

  return result.Item
}

export async function deleteById (table, id) {
  const result = await dynamoDbLib.call('delete', {
    TableName: table,
    Key: {
      id: id
    }
  })
  return result
}

export async function getAll (table) {
  const result = await dynamoDbLib.call('scan', {
    TableName: table
  })
  return result.Items
}

export async function updateById (table, id, data) {
  const dataKeys = Object.keys(data)
  const dataValues = Object.values(data)
  if (dataKeys.length !== dataValues.length) {
    return null
  }
  let updateExpressionString = 'set '
  const updateAttributeValues = {}
  for (let i = 0; dataKeys.length > i; i++) {
    updateExpressionString += `${dataKeys[i]} = :${dataKeys[i]}`
    if (i < dataKeys.length - 1) {
      updateExpressionString += ', '
    }
    updateAttributeValues[`:${dataKeys[i]}`] = dataValues[i]
  }
  const params = {
    TableName: table,
    Key: {
      id: id
    },
    UpdateExpression: updateExpressionString,
    ExpressionAttributeValues: updateAttributeValues,
    ReturnValues: 'ALL_NEW'
  }
  const result = await dynamoDbLib.call('update', params)
  return result.Attributes
}

export async function query (table, IndexName, IndexValue) {
  const result = await dynamoDbLib.call('query', {
    TableName: table,
    IndexName: IndexName,
    KeyConditionExpression: `${IndexName} = :value`,
    ExpressionAttributeValues: {
      ':value': IndexValue
    }
  })
  return result.Items
}

export async function put (table, Item) {
  const result = await dynamoDbLib.call('put', {
    TableName: table,
    Item: Item
  })
  return result
}
