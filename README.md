# README #

### What is this repository for? ###

* Polymetis API Mock
* 1.0.0

### How do I get set up? ###

* yarn install
* serverless offline start
* You can find postman config in mocks/postman
* You can find api doc in the swagger.yml file
* If you get 403 from postman calls, check the value of the serverless offline generated api key

### Who do I talk to? ###

* gregoire.louise@lapilulerouge.io