export function httpFailure (exception) {
  if (!(exception instanceof CustomHttpError)) {
    exception.name = 'UncatchedError'
    exception.statusCode = 500
  }
  return buildHttpError(exception.statusCode, {
    name: exception.name,
    timestamp: Date.now()
  })
}

export async function buildHttpError (statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    body: JSON.stringify(body || {})
  }
}

class CustomHttpError {
  constructor (errorName, statusCode) {
    this.name = errorName
    this.statusCode = statusCode
  }
}

export {
  CustomHttpError
}
