import AWS from 'aws-sdk'

AWS.config.update({ region: process.env.awsRegion })

export function call (action, params) {
  if (process.env.IS_OFFLINE === 'true') {
    const dynamoDb = new AWS.DynamoDB.DocumentClient({
      region: 'localhost',
      endpoint: 'http://localhost:8000',
      accessKeyId: 'DEFAULT_ACCESS_KEY',
      secretAccessKey: 'DEFAULT_SECRET'
    })
    return dynamoDb[action](params).promise()
  } else {
    const dynamoDb = new AWS.DynamoDB.DocumentClient()
    return dynamoDb[action](params).promise()
  }
}
