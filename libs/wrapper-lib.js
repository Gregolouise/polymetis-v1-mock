import { CustomHttpResponse, httpSuccess, buildHttpResponseAWS } from './response-lib';
import { httpFailure } from './errors-lib';

const LPRWrapper = (mode, fn) => async (event, context, callback) => {
  try {
    event.body = event.mock ? event.body : JSON.parse(event.body);
    const result = await fn(event, context, callback);
    switch (mode) {
      case 'http':
        return httpSuccess(result instanceof CustomHttpResponse ? await buildHttpResponseAWS(result) : await buildHttpResponseAWS(new CustomHttpResponse(result, 200)));
    }
    return result;
  } catch (e) {
    console.log('uncatched error', e);
    switch (mode) {
      case 'http':
        return httpFailure(e);
    }
  }
};

const LPRWrapperHttp = (fn) => LPRWrapper('http', fn);

export {
  LPRWrapperHttp
};
