export async function httpSuccess (body) {
  return buildHttpResponse(body.statusCode || 200, body.responseBody);
}

export async function buildHttpResponse (statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    body: statusCode === 204 ? '' : JSON.stringify(body || {})
  };
}

export async function buildHttpResponseAWS (item) {
  return item;
}

class CustomHttpResponse {
  constructor (body, statusCode) {
    this.responseBody = body;
    this.statusCode = statusCode;
  }
}

export {
  CustomHttpResponse
};
